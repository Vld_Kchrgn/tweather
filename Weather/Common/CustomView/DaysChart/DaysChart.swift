//
//  LineChart.swift
//  Weather
//
//  Created by Vlad Kochergin on 06.05.2018.
//  Copyright © 2018 Vlad Kochergin. All rights reserved.
//

import UIKit
import Charts

protocol DaysChartDelegat {
    func daysChart(_ chart: DaysChart, didSelectDay index: Int)
}

final class DaysChart: UIView {
    
    var delegate: DaysChartDelegat?
    private let lineChartView = LineChartView()
    private var chartData = [ChartDataEntry]()
    private(set) var selectedIndex: Int?

    override init(frame: CGRect) {
        super.init(frame: frame)
        initializedView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initializedView()
    }
    
    private func initializedView() {
        lineChartView.translatesAutoresizingMaskIntoConstraints = false
        lineChartView.delegate = self
        addSubview(lineChartView)
        let topConstraint = lineChartView.topAnchor.constraint(equalTo: topAnchor)
        let bottomConstraint = lineChartView.bottomAnchor.constraint(equalTo: bottomAnchor)
        let leftConstraint = lineChartView.leftAnchor.constraint(equalTo: leftAnchor)
        let rightConstraint = lineChartView.rightAnchor.constraint(equalTo: rightAnchor)
        
        NSLayoutConstraint.activate([topConstraint, bottomConstraint, leftConstraint, rightConstraint])
    }
    
    func updateGraph(days: [Day]) {
        chartData = [ChartDataEntry]()
        
        for (index, day) in days.enumerated() {
            let value = ChartDataEntry(x: Double(index), y: day.currentTemp)
            chartData.append(value)
        }
        
        let line = LineChartDataSet(values: chartData, label: "Day")
        line.colors = [UIColor.blue]
        
        let data = LineChartData()
        data.addDataSet(line)
        
        lineChartView.data = data
        lineChartView.chartDescription?.text = "Days"
        lineChartView.zoom(scaleX: 2, scaleY: 0, x: 0, y: 0)
    }
}

extension DaysChart: ChartViewDelegate {
    func chartValueSelected(_ chartView: ChartViewBase, entry: ChartDataEntry, highlight: Highlight) {
        selectedIndex = chartData.index(of: entry)
        if let selectedIndex = selectedIndex {
            delegate?.daysChart(self, didSelectDay: selectedIndex)
        }
    }
}

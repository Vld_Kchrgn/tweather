//
//  Storage.swift
//  Weather
//
//  Created by Vlad Kochergin on 06.05.2018.
//  Copyright © 2018 Vlad Kochergin. All rights reserved.
//

import Foundation

final class Storage {
    
    private func getCacheURL() -> URL {
        if let url = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask).first {
            return url
        } else {
            fatalError("Directory URL error!")
        }
    }
    
    func store<T: Encodable>(_ object: T, as fileName: String) {
        let url = getCacheURL().appendingPathComponent(fileName)
        
        let encoder = JSONEncoder()
        do {
            let data = try encoder.encode(object)
            if FileManager.default.fileExists(atPath: url.path) {
                try FileManager.default.removeItem(at: url)
            }
            FileManager.default.createFile(atPath: url.path, contents: data, attributes: nil)
        } catch {
            fatalError(error.localizedDescription)
        }
    }

    func retrieve<T: Decodable>(_ fileName: String, as type: T.Type) -> T? {
        let url = getCacheURL().appendingPathComponent(fileName)
        
        if !FileManager.default.fileExists(atPath: url.path) {
            return nil
        }
        
        if let data = FileManager.default.contents(atPath: url.path) {
            let decoder = JSONDecoder()
            do {
                let model = try decoder.decode(type, from: data)
                return model
            } catch {
                fatalError(error.localizedDescription)
            }
        } else {
            return nil
        }
    }
}

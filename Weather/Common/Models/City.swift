//
//  City.swift
//  Weather
//
//  Created by Vlad Kochergin on 06.05.2018.
//  Copyright © 2018 Vlad Kochergin. All rights reserved.
//

import Foundation

struct City: Codable {
    let name: String
    let lattitude: String
    let longitude: String
    let days: [Day]
    
    enum CodingKeys: String, CodingKey {
        case name = "city_name"
        case lattitude = "lat"
        case longitude = "lon"
        case days = "data"
    }
}

//
//  CityRequest.swift
//  Weather
//
//  Created by Vlad Kochergin on 06.05.2018.
//  Copyright © 2018 Vlad Kochergin. All rights reserved.
//

import Foundation

struct CityRequestModel: Codable {
    let apiKey = "e02e14b1ff5f4a69b520b87043c7debb"
    let city = "Kiev"
    
    enum CodingKeys: String, CodingKey {
        case apiKey = "key"
        case city
    }
    
    //init - Add if need
}

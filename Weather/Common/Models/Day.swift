//
//  Day.swift
//  Weather
//
//  Created by Vlad Kochergin on 06.05.2018.
//  Copyright © 2018 Vlad Kochergin. All rights reserved.
//

import Foundation

struct Day: Codable {
    let date: Date
    let weather: Weather
    let maxTemp: Double
    let minTemp: Double
    let currentTemp: Double
    
    enum CodingKeys: String, CodingKey {
        case date = "ts"
        case weather = "weather"
        case maxTemp = "max_temp"
        case minTemp = "min_temp"
        case currentTemp = "temp"
    }
}

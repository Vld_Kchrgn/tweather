//
//  Weather.swift
//  Weather
//
//  Created by Vlad Kochergin on 06.05.2018.
//  Copyright © 2018 Vlad Kochergin. All rights reserved.
//

import Foundation

struct Weather: Codable {
    let icon: String
    let code: Int
    let description: String
    var iconURL: URL {
        return URL(string: "https://www.weatherbit.io/static/img/icons")!.appendingPathComponent(icon).appendingPathExtension("png")
    }
}

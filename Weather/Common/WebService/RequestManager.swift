//
//  RequestMapManager.swift
//  Weather
//
//  Created by Vlad Kochergin on 06.05.2018.
//  Copyright © 2018 Vlad Kochergin. All rights reserved.
//

import Foundation
import PromiseKit

final class RequestManager {
    
    private let webService = WebService()
    
    func cityWithWeather(_ request: CityRequestModel) -> Promise<City> {
        let URLrequest = URLRequest(path: "forecast/daily", encodableBody: request)
        return webService.sendRequest(URLrequest)
    }
    
}

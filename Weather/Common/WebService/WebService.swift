//
//  WebService.swift
//  Weather
//
//  Created by Vlad Kochergin on 06.05.2018.
//  Copyright © 2018 Vlad Kochergin. All rights reserved.
//

import Foundation
import PromiseKit
import Alamofire

final class WebService {
    
    @discardableResult
    func sendRequest<T: Decodable>(_ request: URLRequest) -> Promise<T> {
        return Promise<T> { seal in
            
            Alamofire.request(request).responseData { (rawResponse: DataResponse<Data>) in
                switch rawResponse.result {
                case .success(let data):
                    do {
                        let response: T = try self.decode(data)
                        seal.fulfill(response)
                    }
                    catch {
                        seal.reject(error)
                    }
                case .failure(let error):
                    seal.reject(error)
                }
            }
        }
    }
    
    private func decode<T: Decodable>(_ data: Data) throws -> T {
        do {
            let response = try JSONDecoder().decode(T.self, from: data)
            return response
        } catch {
            throw error
        }
    }
}

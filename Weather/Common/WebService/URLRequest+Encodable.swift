//
//  URLRequest+Encodable.swift
//  Weather
//
//  Created by Vlad Kochergin on 06.05.2018.
//  Copyright © 2018 Vlad Kochergin. All rights reserved.
//

import Foundation
import Alamofire

extension URLRequest {
    
    public init<T: Encodable>(
        baseURL: URL = URL(string: "https://api.weatherbit.io/v2.0/forecast/daily")!,
        path: String,
        method: HTTPMethod = .get,
        encodableBody: T? = nil,
        headers: [String: String]? = nil
        ) {
        do {
            try self.init(url: baseURL.appendingPathComponent(path), method: method, headers: headers)
            CodingEncode<T>().encode(&self, parameters: encodableBody)
        } catch {
            fatalError("Failed URLRequest: \(error)")
        }
    }
    
    fileprivate final class CodingEncode<T: Encodable> {
        func encode(_ request: inout URLRequest, parameters: T?) {
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            guard let parameters = parameters else { return }
            
            switch request.httpMethod! {
            case "GET":
                try? getMethod(&request, parameters: parameters)
            default:
                return //Add if need
            }
        }
        
        private func getMethod(_ request: inout URLRequest, parameters: T) throws {
            let urlComponents = NSURLComponents(string: request.url!.absoluteString)!
            do {
                let data = try JSONEncoder().encode(parameters)
                let dictionary = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as! [String: String]
                
                urlComponents.queryItems = dictionary.map({ URLQueryItem(name: $0.key, value: $0.value) })
                request.url = urlComponents.url!
            } catch {
                throw AFError.parameterEncodingFailed(reason: .jsonEncodingFailed(error: error))
            }
        }
    }
    
}

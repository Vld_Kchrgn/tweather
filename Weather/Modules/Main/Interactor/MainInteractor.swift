//
//  MainMainInteractor.swift
//  Weather
//
//  Created by Vlad Kochergin on 06/05/2018.
//  Copyright © 2018 Vlad Kochergin. All rights reserved.
//

import Foundation

final class MainInteractor: MainInteractorInput {
    
    weak var output: MainInteractorOutput!
    private let requestManager = RequestManager()
    private let storage = Storage()
    
    func getCityData() {
        let request = CityRequestModel()
        requestManager.cityWithWeather(request).done { [weak self] (city) in
            guard let strongSelf = self else { return }
            strongSelf.output.reloadCityData(city)
            strongSelf.saveToCache(city)
            }.catch { [weak self] (error) in
                guard let strongSelf = self else { return }
                strongSelf.showDataFromCacheIfAvailable()
                strongSelf.output.showError(error)
        }
    }
    
    private func showDataFromCacheIfAvailable() {
        guard let data = retrieveCityFromCache("lastData") else { return }
        output.reloadCityData(data)
    }
    
    private func saveToCache(_ city: City) {
        let storage = Storage()
        storage.store(city, as: "lastData")
    }
    
    private func retrieveCityFromCache(_ fileName: String) -> City? {
        return storage.retrieve(fileName, as: City.self)
    }
}

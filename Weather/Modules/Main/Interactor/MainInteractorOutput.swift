//
//  MainMainInteractorOutput.swift
//  Weather
//
//  Created by Vlad Kochergin on 06/05/2018.
//  Copyright © 2018 Vlad Kochergin. All rights reserved.
//

import Foundation

protocol MainInteractorOutput: class {
    func reloadCityData(_ data: City)
    func showError(_ error: Error)
}

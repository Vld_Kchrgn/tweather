//
//  MainMainPresenter.swift
//  Weather
//
//  Created by Vlad Kochergin on 06/05/2018.
//  Copyright © 2018 Vlad Kochergin. All rights reserved.
//

import Foundation

final class MainPresenter: NSObject, MainModuleInput, MainViewOutput, MainInteractorOutput {

    weak var view: MainViewInput!
    var interactor: MainInteractorInput!
    var router: MainRouterInput!

    func viewIsReady() {
        self.view.setupInitialState()
    }
    
    func getCityData() {
        interactor.getCityData()
    }
    
    func showError(_ error: Error) {
        view.showError(error)
    }
    
    func reloadCityData(_ data: City) {
        view.reloadCityData(data)
    }
}

//
//  MainMainViewInput.swift
//  Weather
//
//  Created by Vlad Kochergin on 06/05/2018.
//  Copyright © 2018 Vlad Kochergin. All rights reserved.
//

protocol MainViewInput: class {

    /**
        @author Vlad Kochergin
        Setup initial state of the view
    */

    func setupInitialState()
    func reloadCityData(_ data: City)
    func showError(_ error: Error)
}

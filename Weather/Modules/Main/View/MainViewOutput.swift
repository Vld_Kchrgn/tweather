//
//  MainMainViewOutput.swift
//  Weather
//
//  Created by Vlad Kochergin on 06/05/2018.
//  Copyright © 2018 Vlad Kochergin. All rights reserved.
//

protocol MainViewOutput {

    /**
        @author Vlad Kochergin
        Notify presenter that view is ready
    */

    func viewIsReady()
    func getCityData()
}

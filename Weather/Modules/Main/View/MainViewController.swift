//
//  MainMainViewController.swift
//  Weather
//
//  Created by Vlad Kochergin on 06/05/2018.
//  Copyright © 2018 Vlad Kochergin. All rights reserved.
//

import UIKit
import MBProgressHUD

final class MainViewController: UIViewController, MainViewInput {
    
    var output: MainViewOutput!
    
    @IBOutlet private weak var collectionView: UICollectionView!
    @IBOutlet private weak var daysChartView: DaysChart!
    private var cityData: City?
    
    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        output.viewIsReady()
    }
    
    // MARK: MainViewInput
    func setupInitialState() {
        title = "Kiev"
        daysChartView.delegate = self
        regCollectionViewCell()
        updateData()
    }
    
    private func regCollectionViewCell() {
        collectionView.register(UINib(nibName: "DayCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "DayCell")
    }
    
    private func scrollCollectionViewTo(_ index: Int) {
        let indexPath = IndexPath(row: index, section: 0)
        collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
    }
    
    private func updateData() {
        MBProgressHUD.showAdded(to: view, animated: true)
        output.getCityData()
    }
    
    func reloadCityData(_ data: City) {
        MBProgressHUD.hide(for: view, animated: true)
        cityData = data
        daysChartView.updateGraph(days: cityData!.days)
        collectionView.reloadData()
    }
    
    func showError(_ error: Error) {
        MBProgressHUD.hide(for: view, animated: true)
        let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        present(alert, animated: true, completion: nil)
    }
}

extension MainViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let cityData = cityData else {
            return 0
        }
        return cityData.days.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DayCell", for: indexPath) as! DayCollectionViewCell
        let day = cityData!.days[indexPath.row]
        cell.prepareCell(day)
        
        return cell
    }
}

extension MainViewController: DaysChartDelegat {
    func daysChart(_ chart: DaysChart, didSelectDay index: Int) {
        scrollCollectionViewTo(index)
    }
}

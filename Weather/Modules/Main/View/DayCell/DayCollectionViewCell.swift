//
//  DayCollectionViewCell.swift
//  Weather
//
//  Created by Vlad Kochergin on 06.05.2018.
//  Copyright © 2018 Vlad Kochergin. All rights reserved.
//

import UIKit
import SDWebImage

final class DayCollectionViewCell: UICollectionViewCell {

    @IBOutlet private weak var temperatureLabel: UILabel!
    @IBOutlet private weak var weatherIconImagView: UIImageView!
    @IBOutlet private weak var dateLabel: UILabel!
    @IBOutlet private weak var maxTemperatureLabel: UILabel!
    @IBOutlet private weak var minTemperatureLabel: UILabel!
    @IBOutlet private weak var weatherDecriptionLabel: UILabel!
    
    private var dateFormatter: DateFormatter {
        let formatter = DateFormatter()
        formatter.dateFormat = "EEEE, MMM d, yyyy"
        return formatter
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        layer.cornerRadius = 15
        layer.masksToBounds = true
    }
    
    func prepareCell(_ day: Day) {
        temperatureLabel.text = String(Int(day.currentTemp))
        maxTemperatureLabel.text = String(Int(day.maxTemp))
        minTemperatureLabel.text = String(Int(day.minTemp))
        weatherDecriptionLabel.text = day.weather.description
        dateLabel.text = dateFormatter.string(from: day.date)
        
        weatherIconImagView.sd_setImage(with: day.weather.iconURL, completed: nil)
    }

    override func prepareForReuse() {
        weatherDecriptionLabel.text = ""
        weatherIconImagView.image = nil
    }
}

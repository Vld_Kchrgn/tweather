//
//  MainMainInitializer.swift
//  Weather
//
//  Created by Vlad Kochergin on 06/05/2018.
//  Copyright © 2018 Vlad Kochergin. All rights reserved.
//

import UIKit

final class MainModuleInitializer: NSObject {

    //Connect with object on storyboard
    @IBOutlet weak var mainViewController: MainViewController!

    override func awakeFromNib() {

        let configurator = MainModuleConfigurator()
        configurator.configureModuleForViewInput(viewInput: mainViewController)
    }

}
